@component('mail::message')
# Hola {{ $user->name }},

Gracias por crear tu cuenta verificala con el siguente botón.

@component('mail::button', ['url' => route('verify', $user->verification_token ) ])
Confirmar cuenta
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent