@component('mail::message')
# Hola {{ $user->name }},

Has  cambiado tu correo electrónico, verifique nuevamente su cuenta con el siguente botón.

@component('mail::button', ['url' => route('verify', $user->verification_token ) ])
Confirmar cuenta
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
