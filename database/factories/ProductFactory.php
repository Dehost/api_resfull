<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\User;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name'=> $faker->word(),
        'description'=> $faker->paragraph(1),
        'quantity'=> $faker->numberBetween(1,12),
        'status'=> $faker->randomElement([Product::PRODUCT_AVAILABLE, Product::PRODUCT_NOT_AVAILABLE]),
        'image'=> $faker->randomElement(['1.png', '2.png', '3.png']),
        'seller_id'=> User::inRandomOrder()->first()->id,
    ];
});
