<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Buyer;
use App\Product;
use App\Seller;
use App\Transaction;
use Faker\Generator as Faker;

$factory->define(Transaction::class, function (Faker $faker) {
    
    $seller = Seller::has('products')->get()->random(); // Vendedor 
    $buyer = Buyer::all()->except($seller->id)->random(); // Comprador debe ser diferente al vendedor.

    return [
        'quantity'=> $faker->numberBetween(1,3),
        'buyer_id'=> $buyer->id,
        'product_id'=> Product::inRandomOrder()->first()->id,
    ];
});
