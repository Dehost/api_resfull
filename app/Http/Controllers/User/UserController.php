<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Mail\UserCreated;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\ApiController;

class UserController extends ApiController
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return $this->showAll($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'=> 'required',
            'email'=> 'required|email|unique:users',
            'password'=> 'required|min:6|confirmed', // En los datso se envio password_confirmation
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $data['verified'] = User::VERIFIED_NOT_USER;
        $data['verification_token'] = User::tokenGenerate();
        $data['admin'] = User::USER_GUEST;

        $user = User::create($data);
        
        return $this->showOne($user, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return $this->showOne($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules = [
            'email'=> 'email|unique:users,email,'.$user->id,
            'password'=> 'min:6|confirmed', // En los datso se envio password_confirmation
            'admin'=> 'in:' . User::VERIFIED_NOT_USER . ',' . User::VERIFIED_USER,
        ];

        $this->validate($request, $rules);

        if ( $request->has('name') && $user->name != $request->name ) {
            $user->name = $request->name; 
        }

        if ( $request->has('email') && $user->email != $request->email ) {
            $user->email = $request->email;
            $user->verified = User::VERIFIED_NOT_USER;
            $user->verification_token = User::tokenGenerate();
        }

        if ( $request->has('password') ) {
            $user->password = bcrypt($request->password); 
        }

        if ( $request->has('admin') ) {
            if ( !$user->verifiedUser() ) {
                // 422:  peticion mal formada.
                return $this->errorResponse('Unicamente lo usuarios verificados, pueden cambiara a tipo administrador.', 409);
            }
            $request->admin = $request->admin;
        }

        if ( !$user->isDirty() ) {
            return $this->errorResponse('No hay ningun campo para actualizar.', 422);
        }
        
        $user->save();

        return $this->showOne($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return $this->showOne($user, 201);
    }

    public function verify($token)
    {
        $user = User::whereVerificationToken($token)->firstOrFail();

        $user->verified = User::VERIFIED_USER;
        $user->verification_token = null;

        $user->save();

        return $this->showMessage("Cuenta ha sido verificada.");
    }

    public function resend(User $user)
    {
        if ( $user->verifiedUser() ) {
            return $this->errorResponse("El usuario ya esta verificado.", 409);
        }

        retry(3, function() use( $user ) {
            Mail::to($user)->send(new UserCreated($user));
        },100);

        return $this->showMessage("Correo de verificación a sido reenviando.");
    }
}
