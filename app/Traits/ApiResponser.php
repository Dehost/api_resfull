<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

trait ApiResponser 
{
    private function successResponse($data, $code)
    {
        return response()->json($data, $code);
    }
    
    protected function errorResponse($message, $code)
    {
        return response()->json(['error'=> $message, 'code'=>$code], $code);
    }

    protected function showAll(Collection $collection, $code = 200) 
    {
        if( $collection->isEmpty() ) {
            return $this->successResponse(['data'=> $collection], $code);
        }
        
        $transformer = ($collection->first()->transformer) ? $collection->first()->transformer: null;

        $collection = $this->filterData($collection, $transformer);
        $collection = $this->sortData($collection, $transformer);
        $collection = $this->paginate($collection);
        if (  $transformer ) {
            $collection = $this->transformData($collection, $transformer );
        }

        $collection = $this->cacheResponse($collection);

        return $this->successResponse($collection, $code);
    }

    protected function showOne(Model $instance, $code = 200) 
    {
        if( $instance == null ) {
            return $this->successResponse(['data'=> $instance], $code);
        } else if ( $instance->transformer ) {
            $instance =  $this->transformData($instance, $instance->transformer );
        }
        return $this->successResponse($instance, $code);
    }

    protected function showMessage($message, $code = 200) 
    {
        return $this->successResponse(['data'=> $message], $code);
    }

    protected function filterData($collection, $transformer = null)
    {
        foreach (request()->query() as $query => $value) {
            $attribute = $query;
            if ( $transformer ) {
                $attribute = $transformer::originalAttribute($query);
            }

            if ( isset($attribute, $value) ) {
                $collection = $collection->where($attribute, $value);
            }
        }

        return $collection;
    }

    protected function sortData(Collection $collection, $transformer = null) 
    {
        if ( request()->has('sort_by') ) {
            $attribute = request()->sort_by;
            if ( $transformer ) {
                $attribute = $transformer::originalAttribute(request()->sort_by);
            }
            $collection = $collection->sortBy->{$attribute};
        }

        return  $collection;
    }

    protected  function paginate(Collection $collection)
    {

        $rules = [
            "per_page" => "integer|min:1|max:50"
        ];

        Validator::validate(request()->all(), $rules);

        $page = LengthAwarePaginator::resolveCurrentPage();
        
        $perPage = ( request()->has('per_page') ) ? request()->per_page : 15; 

        $results = $collection->slice( ($page-1)* $perPage, $perPage )->values();
        $paginated = new LengthAwarePaginator($results, $collection->count(), $perPage,$page,[
            'path'=> LengthAwarePaginator::resolveCurrentPath()
        ]);
        
        $paginated->appends(request()->all());

        return $paginated;
    }

    protected function transformData($data, $transformer)
    {
        $transformation = fractal($data, new $transformer);

        return $transformation->toArray();
    }

    protected function cacheResponse($data)
    {
        $url = request()->url();

        $queryParams = request()->query();

        ksort($queryParams);

        $queryString = http_build_query($queryParams);

        $fullUrl = "$url?$queryString";

        // 15/60 = 15 segundos.
        return Cache::remember($fullUrl, 15/16, function () use($data) {
            return $data;
        })
    }
}