<?php

namespace App\Exceptions;

use App\Traits\ApiResponser;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    use ApiResponser;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
        /**
         * Manejo de errores de validación
         */
        if ( $exception instanceof ValidationException) {
            return $this->convertValidationExceptionToResponse($exception, $request);
        }

        /**
         * Manejo de errores relacionado con lo modelos no existentes
         */
        if ( $exception instanceof ModelNotFoundException ) {
            $model = strtolower( class_basename( $exception->getModel() ) );
            $id = implode(',',$exception->getIds());
            return $this->errorResponse("No existe una instacia $model para el id($id) expecificado", 404);
        }
        
        /**
         * Manejo de errores de autenticación
         */
        if ( $exception instanceof AuthenticationException ) {
            return $this->unauthenticated($request, $exception);
        }

        /**
         * Manejo de errores de autorización de accion
         */
        if ( $exception instanceof AuthorizationException ) {
            return $this->errorResponse("No tiene permisos para realiza esta acción", 403);
        }

        /**
         * Manejo de errores de url no encontradas.
         */
        if ( $exception instanceof NotFoundHttpException ) {
            return $this->errorResponse("No se encontro la url expeficada.", 404);
        }

        /**
         * Manejo de errores metodo no permitido para la url.
         */
        if ( $exception instanceof MethodNotAllowedHttpException ) {
            return $this->errorResponse("El método expecificado en la petición no es valído.", 405);
        }

        /**
         * Manejo de errores generales realcionados con cualquier petición HTTP.
         */
        if ( $exception instanceof HttpException ) {
            return $this->errorResponse($exception->getMessage(), $exception->getStatusCode());
        }

        /**
         * Manejo de errores generales realcionados con cualquier petición HTTP.
         */
        if ( $exception instanceof QueryException ) {
            $codeError = $exception->errorInfo[1];
            if ( $codeError == 1451 ) {
                return $this->errorResponse('No se puede eliminar el recurso porque está relacionado con algún otro.', 409);
            }
        }

        if ( config('app.debug') ) { // Si esta en modo depuracion render hara su trabajo
            return parent::render($request, $exception);
        }

        return $this->errorResponse('Ocurrio un error inesperado. Intentelo luego', 500);

    }

    /**
     * Create a response object from the given validation exception.
     *
     * @param  \Illuminate\Validation\ValidationException  $e
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        return $this->errorResponse($e->errors(), $e->status);
    }

    /**
     * Convert an authentication exception into a response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $this->errorResponse($exception->getMessage(), 401);
    }

}
