<?php

namespace App\Observers;

use App\Mail\UserCreated;
use App\Mail\UserMailChanged;
use App\User;
use Illuminate\Support\Facades\Mail;

class UserObserver
{
    /**
     * Handle the user "created" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
        //Mail::to($user->email)
        retry(3, function() use( $user ) {
            Mail::to($user)->send(new UserCreated($user));
        },100);
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        if ( $user->isDirty('email') ) {
            retry(3, function() use( $user ) { 
                Mail::to($user)->send(new UserMailChanged($user));
            },100);
        }
    }
}
