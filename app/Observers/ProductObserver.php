<?php

namespace App\Observers;

use App\Product;

class ProductObserver
{
    /**
     * Handle the product "updated" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function updated(Product $product)
    {
        if ( $product->quantity == 0 && $product->available() ) {
            $product->status = Product::PRODUCT_NOT_AVAILABLE;
            $product->save();
        } else if( $product->quantity > 0 && !$product->available() ){
            $product->status = Product::PRODUCT_AVAILABLE;
            $product->save(); 
        }
    }
}
